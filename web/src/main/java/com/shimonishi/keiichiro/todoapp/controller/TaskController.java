/*
 * Copyright 2020 Keiichiro Shimonishi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shimonishi.keiichiro.todoapp.controller;

import com.shimonishi.keiichiro.todoapp.domain.Task;
import com.shimonishi.keiichiro.todoapp.mapper.TodoMapper;
import com.shimonishi.keiichiro.todoapp.service.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/tasks")
@RequiredArgsConstructor
@Slf4j
public class TaskController {
  private final TodoMapper todoMapper;
  private final TaskService taskService;

  @PostMapping("/addNewTask")
  public String addNewTask() {  // TODO set args
    // TODO check params
    // TODO insert into the DB
    return "redirect:/tasks";
  }

  @GetMapping("/createNewTask")
  public String createNewTask(Model model) {
    model.addAttribute("newTask", Task.createNewTask());
    return "tasks/createNewTask";
  }

  @PostMapping("/createNewTask")
  public String registerTask(@Valid @ModelAttribute("newTask") Task newTask,
                             BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      return "tasks/createNewTask";
    }

    todoMapper.insert(newTask);
    return "redirect:/tasks";
  }

  @GetMapping
  public String showAllTasks(Model model) {
    log.info("Showing all tasks.");
    List<Task> tasks = null;
    try {
      log.debug("Starts SQL tasks to fetch all tasks.");
      tasks = todoMapper.selectAll();
      log.debug("Successfully completed the SQL task.");
    } catch (Exception e) {
      log.error("Failed to complete SQL tasks while trying to fetch data.", e);
      return "/error/500";
    }
    model.addAttribute("tasks", tasks);
    return "/tasks/showTasks";
  }

  @GetMapping("/{taskId}")
  public String showDetail(@PathVariable Long taskId, Model model) {
    try {
      taskService.prepareDetailPage(taskId, model);
    } catch (IllegalArgumentException exception) {
      return "/error/400";
    }
    return "/tasks/detail";
  }

  @PostMapping("/updateTask")
  public String updateTask() {  // TODO set args
    // TODO check params
    // TODO update the DB
    return "redirect:/tasks";
  }
}
