/*
 * Copyright 2020 Keiichiro Shimonishi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    id 'checkstyle'
//    id 'com.github.spotbugs'
    id 'java'
}

def springBootVersion = "2.4.0"

group = 'com.shimonishi.keiichiro'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '11'

repositories {
    jcenter()
    mavenCentral()
}

configurations {
    compileOnly {
        extendsFrom annotationProcessor
    }
}

// Use the Checkstyle rules provided by the convention plugin
// Do not allow any warnings
checkstyle {
    config = resources.text.fromString(com.shimonishi.keiichiro.CheckstyleUtil.getCheckstyleConfig("/checkstyle.xml"))
    maxWarnings = 0
}

// Enable deprecation messages when compiling Java code
tasks.withType(JavaCompile).configureEach {
    options.compilerArgs << "-Xlint:deprecation"
}

dependencies {
    compileOnly "org.projectlombok:lombok:1.18.16"

    annotationProcessor "org.projectlombok:lombok:1.18.16"
    annotationProcessor "org.springframework.boot:spring-boot-configuration-processor:${springBootVersion}"

    implementation "ch.qos.logback:logback-core:1.2.3"
    implementation "ch.qos.logback:logback-classic:1.2.3"
    implementation "org.slf4j:slf4j-api:1.7.30"

    implementation enforcedPlatform("org.springframework.boot:spring-boot-dependencies:2.4.0")
    implementation "org.springframework.boot:spring-boot-starter-validation"

    implementation "org.mybatis.spring.boot:mybatis-spring-boot-starter:2.1.4"

    testCompileOnly "org.projectlombok:lombok:1.18.16"
    testAnnotationProcessor "org.projectlombok:lombok:1.18.16"

    testImplementation 'org.springframework.boot:spring-boot-starter-test'
}

test {
    useJUnitPlatform()
}
