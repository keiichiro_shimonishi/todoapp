/*
 * Copyright 2020 Keiichiro Shimonishi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shimonishi.keiichiro.todoapp.logbackfilter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import lombok.Getter;
import lombok.Setter;

public class HikariPoolDebugFilter extends Filter<ILoggingEvent> {

  @Getter @Setter
  private boolean filterPoolSkipped = true;
  @Getter @Setter
  private boolean filterStats = true;

  private static final String MESSAGE_FILL_POOL_SKIPPED =
      "Fill pool skipped, pool is at sufficient level.";
  private static final String MESSAGE_POOL_STATS = "Pool stats (total=";

  @Override
  public FilterReply decide(ILoggingEvent event) {
    if (!event.getLevel().equals(Level.DEBUG)) {  // filter only logs at DEBUG level
      return FilterReply.NEUTRAL;
    }

    if (filterFillPoolSkipped(event)) {
      return FilterReply.DENY;
    }
    if (filterPoolStats(event)) {
      return FilterReply.DENY;
    }

    return FilterReply.NEUTRAL;
  }

  private boolean filterFillPoolSkipped(ILoggingEvent event) {
    if (!filterPoolSkipped) {
      return false;
    }
    return event.getFormattedMessage().contains(MESSAGE_FILL_POOL_SKIPPED);
  }

  private boolean filterPoolStats(ILoggingEvent event) {
    if (!filterStats) {
      return false;
    }
    return event.getFormattedMessage().contains(MESSAGE_POOL_STATS);
  }

}
