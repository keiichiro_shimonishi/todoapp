/*
 * Copyright 2020 Keiichiro Shimonishi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shimonishi.keiichiro.todoapp.domain;

import com.shimonishi.keiichiro.todoapp.common.Constants;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

@Setter
@Getter
public class Task implements Serializable {

  private static final long serialVersionUID = 1L;

  private int taskId;

  @NotNull
  @Size(min=1, max=50)
  private String title;

  private String detail;

  @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm")
  @NotNull
  @Setter(AccessLevel.NONE)
  private Date dueWithoutTimeZone;

  @Setter(AccessLevel.NONE)
  private OffsetDateTime dueWithTimeZone;

  private boolean archiveFlag;

  private boolean completeFlag;

  public Task() {}

  public Task(int taskId, @NotNull @Size(min = 1, max = 50) String title, String detail,
              @NotNull Date dueWithoutTimeZone, boolean archiveFlag, boolean completeFlag) {
    this.taskId = taskId;
    this.title = title;
    this.detail = detail;
    this.dueWithoutTimeZone = dueWithoutTimeZone;
    this.dueWithTimeZone =
        dueWithoutTimeZone.toInstant().atOffset(ZoneOffset.of(Constants.OFFSET_TIME_IN_HOURS_STR));
    this.archiveFlag = archiveFlag;
    this.completeFlag = completeFlag;
  }

  public Task(int taskId, @NotNull @Size(min = 1, max = 50) String title, String detail,
              OffsetDateTime dueWithTimeZone, boolean archiveFlag, boolean completeFlag) {
    this.taskId = taskId;
    this.title = title;
    this.detail = detail;
    this.dueWithTimeZone = dueWithTimeZone;
    this.dueWithoutTimeZone = Date.from(dueWithTimeZone.toInstant());
    this.archiveFlag = archiveFlag;
    this.completeFlag = completeFlag;
  }

  public static Task createNewTask() {
    return new Task(-1, "", "", OffsetDateTime.now(), false, false);
  }

  public String displayDateJst() {
    return dueWithTimeZone.format(DateTimeFormatter.ofPattern("yyyy年 MM月 dd日 HH:mm", Locale.JAPAN));
  }

  public String displayCompletionStatusJa() {
    return completeFlag ? "完了" : "未";
  }

  public void setDueWithoutTimeZone(Date dueWithoutTimeZone) {
    this.dueWithoutTimeZone = dueWithoutTimeZone;
    this.dueWithTimeZone =
        dueWithoutTimeZone.toInstant().atOffset(ZoneOffset.of(Constants.OFFSET_TIME_IN_HOURS_STR));
  }

  public void setDueWithTimeZone(OffsetDateTime dueWithTimeZone) {
    this.dueWithTimeZone = dueWithTimeZone;
    this.dueWithoutTimeZone = Date.from(dueWithTimeZone.toInstant());
  }
}
