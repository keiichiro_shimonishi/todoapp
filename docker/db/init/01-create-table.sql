DROP TABLE IF EXISTS todo;

CREATE TABLE todo
(
    todo_id   SERIAL PRIMARY KEY,
    title     VARCHAR(64)              NOT NULL,
    detail    TEXT,
    due       TIMESTAMP WITH TIME ZONE NOT NULL,
    archived  BOOLEAN DEFAULT false,
    completed BOOLEAN DEFAULT false
);
