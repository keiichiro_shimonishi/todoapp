/*
 * Copyright 2021 Keiichiro Shimonishi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

INSERT INTO todo(title, due, archived, completed)
VALUES ('テストタスク1', TIMESTAMP '2020-10-05 23:59:59+09', false, false);
INSERT INTO todo(title, due, archived, completed)
VALUES ('テストタスク2', TIMESTAMP '2020-10-06 23:59:59+09', false, true);
INSERT INTO todo(title, due, archived, completed)
VALUES ('テストタスク3', TIMESTAMP '2020-10-07 23:59:59+09', true, false);
INSERT INTO todo(title, due, archived, completed)
VALUES ('テストタスク4', TIMESTAMP '2020-10-08 23:59:59+09', true, true);
